#include <stdio.h>

int main() {
  int height, width, area, perimeter;

  height = 3;
  width = 5;
  area = width * height;
  perimeter = 2 * (width + height);

  printf("Perimeter of the rectangle = %d", perimeter);
  printf("\nArea of the rectangle = %d", area);
  return 0;
}
