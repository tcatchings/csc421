#include <stdio.h>

int main() {
  int a = 125, b = 12345;
  long ax = 1234567890;
  short s = 4043;
  float x = 2.13459;
  double dx = 1.1415927;
  char c = 'W';
  unsigned long ux = 2541567890;

  printf("a + c = %d", a + c);
  printf("\nx + c = %d", x + c);
  printf("\ndx + x = %d", dx + x);
  printf("\n((int) dx) + ax = 1 %d", ((int) dx) + ax);
  printf("\na + x = %d", a + x);
  printf("\ns + b = %d", s + b);
  printf("\nax + b = %d", ax + b);
  printf("\ns + c = %d", s + c);
  printf("\nax + c = %d", ax + c);
  printf("\nax + ux = %d", ax + ux);

  return 0;
}
