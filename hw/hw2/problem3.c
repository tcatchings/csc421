#include <stdlib.h>
#include <stdio.h>

double get_number() {
  printf("Type a number: ");
  char input[10];
  fgets(input, 9, stdin);
  return atof(input);
}

int main() {
  double numbers[3];
  numbers[0] = get_number();
  numbers[1] = get_number();
  numbers[2] = get_number();
  double sum;
  printf("Array [");
  for(int i = 0; i < 3; i++) {
    printf("%f, ", numbers[i]);
    sum += numbers[i];
  }
  printf("].\n Sum of the array is %f.\n", sum);
}