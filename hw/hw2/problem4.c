#include <stdlib.h>
#include <stdio.h>

double get_number() {
  printf("Type a number: ");
  char input[10];
  fgets(input, 9, stdin);
  return atof(input);
}

void get_array(double* numbers) {
  numbers[0] = get_number();
  numbers[1] = get_number();
  numbers[2] = get_number();
}

int main() {
  double numbers[2][3];
  double combined[6];
  printf("Type in the first array's values:\n");
  get_array(numbers[0]);
  printf("Type in the second array's values:\n");
  get_array(numbers[1]);
  for(int i = 0, counter = 0; i < 2; i++) {
    for(int j = 0; j < 3; j++, counter++) {
      combined[counter] = numbers[i][j];
    }
  }
  printf("Combined array [");
  for(int i = 0; i < 6; i++) {
    printf("%f, ", combined[i]);
  }
  printf("].\n");
}