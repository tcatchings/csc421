#include <stdio.h>

int main () {
  int n = 0;
  int sum = 0;
  printf("The first 10 natural numbers are :\n");
  while (n < 10) {
    n++;
	printf("%d ", n);
	sum += n;
  }
  printf("\nThe sum is: %d\n", sum);
return 0;
}