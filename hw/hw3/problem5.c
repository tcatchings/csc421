#include <stdio.h>

double square(double num){
  return num * num;
}

int main() {
  char buff [1];
  printf("Input any number for square : ");
  fgets(buff, 1, stdin);
  printf("\nThe square of %d is : %.2f\n", atoi(buff), square(atof(buff)));
  return 0;
}