#include <stdio.h>

int main() {
    int pop(node_t ** head) {
        int retval = -1;
        node_t * next_node = NULL;
    
        if (*head == NULL) {
            return -1;
        }
    
        next_node = (*head)->next;
        retval = (*head)->val;
        free(*head);
        *head = next_node;
    
        return retval;
    }
    

return 0;
}
