#include <stdio.h>

int main() {
    int remove_by_index(node_t ** head, int n) {
        int i = 0;
        int retval = -1;
        node_t * current = *head;
        node_t * temp_node = NULL;
    
        if (n == 0) {
            return pop(head);
        }
    
        for (int i = 0; i < n-1; i++) {
            if (current->next == NULL) {
                return -1;
            }
            current = current->next;
        }
    
        temp_node = current->next;
        retval = temp_node->val;
        current->next = temp_node->next;
        free(temp_node);
    
        return retval;
    
    }
    

return 0;
}
